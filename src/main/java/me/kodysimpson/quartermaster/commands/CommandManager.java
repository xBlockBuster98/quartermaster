package me.kodysimpson.quartermaster.commands;

import me.kodysimpson.quartermaster.commands.subcommands.*;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CommandManager implements CommandExecutor {

    private ArrayList<SubCommand> subcommands = new ArrayList<>();

    public CommandManager(){
        //Get the subcommands so we can access them in the command manager class(here)
        subcommands.add(new LockCommand());
        subcommands.add(new ManageCommand());
        subcommands.add(new HelpCommand());
        subcommands.add(new ReloadCommand());
        subcommands.add(new AdminCommand());
        subcommands.add(new RemoveCommand());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player){
            Player p = (Player) sender;

                if (args.length == 0){
                    HelpCommand help = new HelpCommand();
                    help.perform(p, args);
                }else if(args.length > 0){
                    boolean isValidSubcommand = false;
                    for (int i = 0; i < this.getSubCommands().size(); i++){
                        if (args[0].equalsIgnoreCase(this.getSubCommands().get(i).getName())){
                            isValidSubcommand = true;
                            //The subcommand matches the argument given in /qm [subcommand]
                            this.getSubCommands().get(i).perform(p, args);

                            break;
                        }
                    }
                    if (!isValidSubcommand){
                        p.sendMessage(ChatColor.RED + "That is not a valid command.");
                        p.sendMessage(ChatColor.GRAY + "Do " + ChatColor.YELLOW + "/qm help" + ChatColor.GRAY + " for more info.");
                    }
                    return true;
                }

        }

        return true;
    }

    public ArrayList<SubCommand> getSubCommands(){
        return subcommands;
    }
}
