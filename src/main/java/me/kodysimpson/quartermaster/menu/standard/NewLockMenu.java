package me.kodysimpson.quartermaster.menu.standard;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class NewLockMenu extends Menu {

    @Override
    public String getMenuName() {
        return "QM > Lock Block?";
    }

    @Override
    public int getSlots() {
        return 9;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();

        switch(e.getCurrentItem().getType()){
            case TOTEM_OF_UNDYING:
                p.sendMessage(ChatColor.GRAY + "Creating lock...");

                //Get the block we had stored and store it officially as a lock in the DB
                LockUtils.createNewLock(p, playerMenuUtility.getLockToCreate());

                p.sendMessage(ChatColor.GREEN + "Lock Created!");

                p.closeInventory();
                break;
            case BARRIER:
                p.sendMessage(ChatColor.GREEN + "OK! This will remain unlocked.");
                p.closeInventory();
                break;
        }

    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        ItemStack yes = new ItemStack(Material.TOTEM_OF_UNDYING, 1);
        ItemMeta yes_meta = yes.getItemMeta();
        yes_meta.setDisplayName(ChatColor.GREEN + "Yes");
        yes.setItemMeta(yes_meta);

        ItemStack no = new ItemStack(Material.BARRIER, 1);
        ItemMeta no_meta = no.getItemMeta();
        no_meta.setDisplayName(ChatColor.DARK_RED + "No");
        no.setItemMeta(no_meta);

        inventory.setItem(3, yes);
        inventory.setItem(5, no);

        setFillerGlass();
    }

}
