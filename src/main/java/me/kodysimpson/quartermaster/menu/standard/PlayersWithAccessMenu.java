package me.kodysimpson.quartermaster.menu.standard;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;

public class PlayersWithAccessMenu extends Menu {
    @Override
    public String getMenuName() {
        return "QM > Players with Access";
    }

    @Override
    public int getSlots() {
        return 45;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            new AccessManagerMenu().open(playerMenuUtility.getP());
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {
        //Retrieve all of the players with access to the lock
        ArrayList<OfflinePlayer> accessList = (ArrayList<OfflinePlayer>) LockUtils.getAccessListFromID(playerMenuUtility.getLockID());

        if (!(accessList.isEmpty())) {
            for (int i = 0; i < accessList.size(); i++) {
                UUID uuid = accessList.get(i).getUniqueId();
                OfflinePlayer p2 = Bukkit.getOfflinePlayer(uuid);

                ItemStack player = new ItemStack(Material.PLAYER_HEAD, 1);
                ItemMeta player_meta = player.getItemMeta();
                player_meta.setDisplayName(ChatColor.GREEN + p2.getName());
                player.setItemMeta(player_meta);

                inventory.addItem(player);
            }
        }

        ItemStack close = new ItemStack(Material.BARRIER, 1);
        ItemMeta close_meta = close.getItemMeta();
        close_meta.setDisplayName(ChatColor.DARK_RED + "Close");
        close.setItemMeta(close_meta);

        inventory.setItem(44, close);

        setFillerGlass();
    }
}
