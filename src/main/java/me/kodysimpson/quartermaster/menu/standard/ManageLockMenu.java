package me.kodysimpson.quartermaster.menu.standard;

import me.kodysimpson.quartermaster.menu.Menu;
import me.kodysimpson.quartermaster.menu.PlayerMenuUtility;
import me.kodysimpson.quartermaster.model.Lock;
import me.kodysimpson.quartermaster.utils.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ManageLockMenu extends Menu {

    @Override
    public String getMenuName() {
        return "QM > Manage Lock";
    }

    @Override
    public int getSlots() {
        return 9;
    }

    @Override
    public void handleMenu(InventoryClickEvent e, PlayerMenuUtility playerMenuUtility) {
        Player p = playerMenuUtility.getP();
        if (e.getCurrentItem().getType().equals(Material.BARRIER)) {
            //go back to the previous menu
            new LocksListMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.WITHER_ROSE)) {

            //Open up a confirm menu to delete lock
            new ConfirmDeleteMenu().open(p);
        } else if (e.getCurrentItem().getType().equals(Material.ARMOR_STAND)) {
            //Open the AccessManager
            new AccessManagerMenu().open(p);
        }
    }

    @Override
    public void setMenuItems(PlayerMenuUtility playerMenuUtility) {

        ItemStack manage_access = new ItemStack(Material.ARMOR_STAND, 1);
        ItemMeta access_meta = manage_access.getItemMeta();
        access_meta.setDisplayName(ChatColor.YELLOW + "Access Manager");
        ArrayList<String> access_lore = new ArrayList<>();
        access_lore.add(ChatColor.GREEN + "Manage who has access to this lock");
        access_meta.setLore(access_lore);
        manage_access.setItemMeta(access_meta);

        ItemStack delete_lock = new ItemStack(Material.WITHER_ROSE, 1);
        ItemMeta delete_meta = delete_lock.getItemMeta();
        delete_meta.setDisplayName(ChatColor.DARK_RED + "Delete Lock");
        ArrayList<String> delete_lore = new ArrayList<>();
        delete_lore.add(ChatColor.GREEN + "Deleting the lock will ");
        delete_lore.add(ChatColor.GREEN + "make the block totally unprotected.");
        delete_meta.setLore(delete_lore);
        delete_lock.setItemMeta(delete_meta);

        ItemStack totem = new ItemStack(Material.TOTEM_OF_UNDYING, 1);
        ItemMeta totem_meta = totem.getItemMeta();
        totem_meta.setDisplayName(ChatColor.GOLD + "Lock Manager");
        totem.setItemMeta(totem_meta);

        ItemStack lock_info = new ItemStack(Material.WRITABLE_BOOK, 1);
        ItemMeta info_meta = lock_info.getItemMeta();
        info_meta.setDisplayName(ChatColor.GREEN + "Lock Information");
        ArrayList<String> info_lore = new ArrayList<>();

        info_lore.add(ChatColor.GOLD + "-------------");
        info_lore.add(ChatColor.YELLOW + "Location:");
        Location location = LockUtils.getLockLocation(playerMenuUtility.getLockID());

        Lock lock = LockUtils.getLock(playerMenuUtility.getLockID());
        info_lore.add(ChatColor.AQUA + "  x: " + ChatColor.GREEN + location.getBlockX());
        info_lore.add(ChatColor.AQUA + "  y: " + ChatColor.GREEN + location.getBlockY());
        info_lore.add(ChatColor.AQUA + "  z: " + ChatColor.GREEN + location.getBlockZ());
        info_lore.add("Date Created: " + lock.getCreationDate().toString());
        info_lore.add(ChatColor.GOLD + "-------------");
        info_lore.add(lock.getLockID());

        info_meta.setLore(info_lore);
        lock_info.setItemMeta(info_meta);

        ItemStack close_menu = new ItemStack(Material.BARRIER, 1);
        ItemMeta close_meta = close_menu.getItemMeta();
        close_meta.setDisplayName(ChatColor.DARK_RED + "Close");
        ArrayList<String> close_lore = new ArrayList<>();
        close_lore.add(ChatColor.GREEN + "Go back the locks list");
        close_meta.setLore(close_lore);
        close_menu.setItemMeta(close_meta);

        //set the slots for the options
        inventory.setItem(0, manage_access);
        inventory.setItem(1, delete_lock);
        inventory.setItem(4, totem);
        inventory.setItem(7, lock_info);
        inventory.setItem(8, close_menu);

        setFillerGlass();
    }
}
